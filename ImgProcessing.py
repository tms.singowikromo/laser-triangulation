import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
import csv
import math


#Deze onderstaande code wordt de foto geimporteerd en zwart-wit gemaakt voor analyse.

img_raw = cv2.imread("image6.jpg")
img_BGR2GRAY = cv2.cvtColor(img_raw, cv2.COLOR_BGR2GRAY)
# ret, img_OTSU = cv2.threshold(img_BGR2GRAY, 0, 255, cv2.THRESH_OTSU)
ret, thresh1 = cv2.threshold(img_BGR2GRAY, 180, 255, cv2.THRESH_BINARY)
cv2.imwrite('image5.jpg', thresh1)


#In het volgende deel van de code wordt de afbeelding gescand.  ‘step_size’ geeft aan hoe groot de stappen die het programma maakt. 
#In ons geval, 1, scant de software de afbeelding pixel voor pixel. Vervolgens wordt de loop aangemaakt die voor elke pixel van de breedte gaat lopen. 
#Op lijn 30 wordt de afbeelding pixel voor pixel gesneden. Op lijn 32 wordt white_line aangemaakt, white_line bestaat alleen uit pixels die volledig wit zijn, 
#en dus de laserlijn selecteren. Vervolgens worden de pixels verdeeld in hun eigen lijsten: een lijst voor de x waarden en y-waarden. 
#Vervolgens wordt de x-waarde negatief gemaakt. Dit wordt gedaan omdat de huidige 0 waarde van de x het verste punt van de camera is, 
#terwijl dadelijk de afstand vanaf de camera berekend moet worden, daarom moet de x-waarde afnemen en niet toenemen. 
#De mediaan van beide values wordt gebruikt omdat de laserlijn meerdere pixels kan bezetten, door de mediaan van deze waardes te gebruiken, 
#gebruik je het middelpunt van de laserlijn.

step_size = 1
x_coord = np.zeros(990) #Y-Coordinaten van de foto
y_coord = np.zeros(990) #X-Coordinaten van de foto
linear_formula_line = np.zeros(990)
distance_line_bend = np.zeros(990)
 
for i in range(0, 990, 1): #scannen de afbeelding per pixel
    crop_img = thresh1[0:780, i:step_size]
    step_size = step_size + 1
    
    white_line = np.argwhere(crop_img == 255) #filter de witte lijn, de laser
 
    xval = [pixel[0] for pixel in white_line]
    yval = [pixel[1] for pixel in white_line]
 
    x_val_e = -1 * np.median(xval) #dit is y -> x_coord #inverted?
    y_val_e = np.median(yval) #dit is x -> y_coord #inverted?
    
    a = 0
    x_coord[i] = x_val_e  #Dit is y
    y_coord[i] = (y_val_e + i) #Dit is x
 
# print(x_coord)
# print(y_coord)
#print(y_coord[1])
#Bepaling base line
y_1 = x_coord[1]
y_2 = x_coord[988] #eerst 1090
 
x_1 = y_coord[1]
x_2 = y_coord[988] #eerst 1090
 
a = ((y_2 - y_1)/(x_2 - x_1)) #quotient
b_graf = y_1 - (a * x_1)

#diagnostics
print("y_1: " + str(y_1))
print("y_2: " + str(y_2))
print("x_1: " + str(x_1))
print("x_2: " + str(x_2))
print("quotient: " + str(a))
print("b_graf: " + str(b_graf))

clean_x_coord = [x for x in x_coord if str(x) != 'nan']
clean_y_coord = [x for x in y_coord if str(x) != 'nan']


#In lijn 62 en 63 wordt de PFC berekend door op lijn 62 een onzichtbare lijn te trekken onder de buigingen van het object. 
#Vervolgens worden deze waardes in lijn 63 van elkaar afgetrokken om het verschil (de PFC) te kunnen gebruiken. 
#In lijn 66, 67 en 68 wordt formule 4.1[13]. Uitgevoerd. Het resultaat wordt vervolgens x10 gedaan om de resulterende waarde in millimeter te krijgen.
#Op lijn 71 tot 74 wordt een lijst gemaakt met de dieptegegevens zodat hier later de stelling van pythagoras uitgevoerd kan worden.
#Op lijn 77 wordt de metrische waarde van de Y berekend, dit wordt gedaan door de breedte van de foto in centimeter te delen door het aantal pixels. 
#Vervolgens wordt dit x10 gedaan om millimeter te krijgen.

t7 = np.zeros(988) #Werkelijke Y-coordinaat voor XYZ-bestand
t8 = np.zeros(988) #werkelijke Z-coordinaat voor XYZ-bestand
Object_afstand = np.zeros(988)
 
#Bepaling hoogte
for k in range(0, len(clean_y_coord), 1): #eerst 1090
    #Bepalen pfc
    linear_formula_line[k] = (a * clean_y_coord[k]) + b_graf
    distance_line_bend[k] = clean_x_coord[k] - linear_formula_line[k]
    #Bepaling hoogte

    theta = 0.00005 * distance_line_bend[k] + 0.0394 #deze moet veranderd worden
    tan_theta = math.tan(theta)
    Object_afstand[k] = (60 - (16/tan_theta)) * 10 #hoogtebepaling -> gecalibreerd -> 

    #Elke waarde onder 0 heeft geen hoogte
    if Object_afstand[k] <= 0:
        t8[k] = 0
    else:
        t8[k] = Object_afstand[k]
   
    #Bepaling echte Y-Coordinate
    t7[k] = (y_coord[k] * (29/990)) * 10 #29 is breedte van stelling tot stelling hout 

#print(linear_formula_line)
#print(distance_line_bend)
clean_linear_formula_line = [x for x in linear_formula_line if str(x) != 'nan']
clean_distance_line_bend = [x for x in distance_line_bend if str(x) != 'nan']
#print(clean_linear_formula_line)
#print(clean_distance_line_bend)

#plt.plot(y_coord,x_coord)
t7_nums = [x for x in t7 if str(x) != 'nan']
t8_nums = [x for x in t8 if str(x) != 'nan']
#print(t7_nums)
#print(t8_nums)
#plt.plot(t7_nums,t8_nums)
#plt.show()

def BoogLengte(t7, t8):
    cumulat_boog = 0
    for i in range(0, len(t7)):
        # if t8[i] != 0 and math.isnan(t8[i]) == False and math.isnan(t7[i]) == False:
            cumulat_boog += math.sqrt((t7[i]**2) + (t8[i]**2))
 
    print(cumulat_boog)

BoogLengte(t7_nums, t8_nums) #t7